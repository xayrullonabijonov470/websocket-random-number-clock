let randomNumberClient = null;


function updateClock(random) {
	$("#random").text(random);
}

function connect() {
	var socket = new SockJS('/websocket-random');
	randomNumberClient = Stomp.over(socket);
	randomNumberClient.connect({}, function(frame) {
		randomNumberClient.subscribe('/topic/random', function(message) {
			updateClock(JSON.parse(message.body).random);
		});
		forceUpdate();
	});
}


function forceUpdate() {
	randomNumberClient.send("/app/random", {}, JSON.stringify({}));
}

$(function() {
	connect();
	$( "#update" ).click(function() { forceUpdate(); });
})
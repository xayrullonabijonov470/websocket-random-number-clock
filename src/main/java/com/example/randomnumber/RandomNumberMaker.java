package com.example.randomnumber;

import org.springframework.stereotype.Component;

@Component
public class RandomNumberMaker {
	private long value=999_999_971;
	public RandomNumber makeRandomNumber() {

		return new RandomNumber((value++)+"$");
	}

	public void makeZero() {
		value=0;
	}
}

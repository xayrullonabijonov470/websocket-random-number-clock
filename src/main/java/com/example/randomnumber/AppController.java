package com.example.randomnumber;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@EnableScheduling
@RequiredArgsConstructor
public class AppController {

	@GetMapping("/")
	public String home() {
		return "home";
	}
	private final RandomNumberMaker randomNumberMaker;

	@MessageMapping("/random")
	@SendTo("/topic/random")
	public RandomNumber time() {
		return randomNumberMaker.makeRandomNumber();
	}
}

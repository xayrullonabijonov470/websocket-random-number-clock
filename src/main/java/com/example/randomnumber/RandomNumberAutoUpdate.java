package com.example.randomnumber;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
@RequiredArgsConstructor
public class RandomNumberAutoUpdate {
    private final RandomNumberMaker randomNumberMaker;
    private final SimpMessagingTemplate simpMessagingTemplate;

    @Scheduled(fixedDelay = 1_000)
    public void tick() {
        simpMessagingTemplate.convertAndSend("/topic/random", randomNumberMaker.makeRandomNumber());
    }

    @Scheduled(cron = "50 * * * * ?")
    public void afterOneHourZero() {
        randomNumberMaker.makeZero();
    }

}
